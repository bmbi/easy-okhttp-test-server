package com.mzlion.test.easyokhttp.server.config;

import com.mzlion.core.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;

/**
 * Created by mzlion on 2017/3/23.
 */
@Configuration
public class TomcatConfig {

    @Value("${custom.tomcat.baseDir}")
    private String baseDir;
    @Value("${custom.tomcat.docRoot}")
    private String docRoot;
    @Value("${custom.tomcat.contextPath}")
    private String contextPath;
    @Value("${custom.tomcat.port}")
    private int port;

    @Bean
    public EmbeddedServletContainerFactory servletContainer() {
        File baseDir = new File(this.baseDir);
        FileUtils.forceMakeDir(baseDir);
        File documentRoot = new File(baseDir, this.docRoot);
        FileUtils.forceMakeDir(documentRoot);
        TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory(this.contextPath, this.port);
        tomcat.setBaseDirectory(baseDir);
        tomcat.setDocumentRoot(documentRoot);
        return tomcat;
    }
}
