package com.mzlion.test.easyokhttp.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

/**
 * 程序入口
 *
 * @author mzlion on 2016/12/9.
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) throws IOException {
        //start spring boot.
        SpringApplication.run(Application.class, args);
    }

}
