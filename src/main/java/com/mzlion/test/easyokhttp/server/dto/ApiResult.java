package com.mzlion.test.easyokhttp.server.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author mzlion on 2016/12/10.
 */
public class ApiResult<E> implements Serializable {

    private static final long serialVersionUID = 3647157236558764770L;

    private int code = 0;
    private String errorMessage;
    private E data;

    private Map<String,List<FileData>> fileData;
    private Map<String, String> headers;

    public ApiResult() {
    }

    public ApiResult(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public E getData() {
        return data;
    }

    public void setData(E data) {
        this.data = data;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public Map<String, List<FileData>> getFileData() {
        return fileData;
    }

    public void setFileData(Map<String, List<FileData>> fileData) {
        this.fileData = fileData;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ApiResult{");
        sb.append("code=").append(code);
        sb.append(", errorMessage='").append(errorMessage).append('\'');
        sb.append(", data=").append(data);
        sb.append(", fileData=").append(fileData);
        sb.append(", headers=").append(headers);
        sb.append('}');
        return sb.toString();
    }
}
