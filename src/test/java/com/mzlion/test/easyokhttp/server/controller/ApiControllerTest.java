package com.mzlion.test.easyokhttp.server.controller;

import com.mzlion.core.http.ContentType;
import com.mzlion.core.json.TypeRef;
import com.mzlion.easyokhttp.HttpClient;
import com.mzlion.test.easyokhttp.server.dto.ApiResult;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * Created by mzlion on 2017/3/23.
 */
public class ApiControllerTest {


    @Test
    public void getIpInfo() throws Exception {
        ApiResult<Map<String, Object>> ipInfo = HttpClient
                .get("https://project.mzlion.com/easy-okhttp/api/ip-info")
//                .queryString("ip", "112.64.217.29")
                .asBean(new TypeRef<ApiResult<Map<String, Object>>>() {
                });
        System.out.println("ipInfo = " + ipInfo);
    }

    @Test
    public void simplePost() throws Exception {
        ApiResult apiResult = HttpClient
                .post("https://project.mzlion.com/easy-okhttp/api/post/simple")
                .param("blog", "https://www.mzlion.com")
                .param("author", "mzlion")
                .param("location", "上海")
                .param("projectName", "easy-okhttp")
                .param("projectUrl", "https://git.oschina.net/mzllon/easy-okhttp")
                .header("customHeader1", "customValue1")//自定义header
                .asBean(ApiResult.class);
        System.out.println("apiResult = " + apiResult);
    }

    @Test
    public void formPost() throws Exception {
        ApiResult apiResult = HttpClient
                .post("https://project.mzlion.com/easy-okhttp/api/post/form")
                .param("author", "mzlion")
                .param("projectUrl", "https://git.oschina.net/mzllon/easy-okhttp")
                .param("projectName", "easy-okhttp")
                .param("habit", "电影")
                .param("habit", "编程")
                .param("habit", "睡觉")
                .param("logo", this.getClass().getClassLoader().getResourceAsStream("lavender.jpg"), "lavender.jpg")
                .header("customHeader1", "customValue1")//自定义header
                .asBean(ApiResult.class);
        System.out.println("apiResult = " + apiResult);
    }

    @Test
    public void rawBodyPost() throws Exception {
//        Map<String, Object> personal = new HashMap<>();
//        personal.put("name", "mzlion");
//        personal.put("site", "https://www.mzlion.com");
//        personal.put("location", "上海");
//
//        List<Map<String, String>> projects = new ArrayList<>();
//        Map<String, String> project1 = new HashMap<>();
//        project1.put("url", "https://git.oschina.net/mzllon/easy-okhttp");
//        project1.put("name", "easy-okhttp");
//        projects.add(project1);
//        Map<String, String> project2 = new HashMap<>();
//        project2.put("url", "https://git.oschina.net/mzllon/mzlion-core");
//        project2.put("name", "mzlion-core");
//        projects.add(project2);
//        personal.put("projectList", projects);
//        System.out.println(JsonUtil.toJson(personal, false));

        ApiResult apiResult = HttpClient
                .textBody("https://project.mzlion.com/easy-okhttp/api/post/body/raw")
                .json("{\"projectList\":[{\"name\":\"easy-okhttp\",\"url\":\"https://git.oschina.net/mzllon/easy-okhttp\"},{\"name\":\"mzlion-core\",\"url\":\"https://git.oschina.net/mzllon/mzlion-core\"}],\"site\":\"https://www.mzlion.com\",\"name\":\"mzlion\",\"location\":\"上海\"}")
                .asBean(ApiResult.class);

        System.out.println("apiResult = " + apiResult);
    }

    @Test
    public void binaryBodyPost() throws Exception {
        ByteArrayInputStream bais = new ByteArrayInputStream("{\"projectList\":[{\"name\":\"easy-okhttp\",\"url\":\"https://git.oschina.net/mzllon/easy-okhttp\"},{\"name\":\"mzlion-core\",\"url\":\"https://git.oschina.net/mzllon/mzlion-core\"}],\"site\":\"https://www.mzlion.com\",\"name\":\"mzlion\",\"location\":\"上海\"}"
                .getBytes(Charset.forName("UTF-8")));
        ApiResult<String> apiResult = HttpClient
                .binaryBody("https://project.mzlion.com/easy-okhttp/api/post/body/binary")
                .contentType(ContentType.DEFAULT_BINARY)
                .stream(bais)
                .asBean(new TypeRef<ApiResult<String>>() {
                });

        System.out.println("apiResult = " + apiResult);
    }

}