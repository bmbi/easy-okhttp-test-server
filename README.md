# easy-okhttp-test-server

`easy-okhttp-test-server`是为软件[easy-okhttp](https://git.oschina.net/mzllon/easy-okhttp)编写的一个demo，使用`spring-boot`服务模拟后台，`src/test`下模拟客户端。所以这个demo即包含了服务端也包含了客户端。

这个项目系的比较简单，内容比较单一，主要是没想好怎么丰满它，所以大家凑合的看吧，如果大家有好的点子，可以提`issue`。
其次项目采用了时下很流行的`spring-boot`，目的希望自己尝试使用下，使用后的感受还好：减少了相关配置，减少了pom依赖管理，开箱即用，但是想要用的溜还是需要深入学习。

## 使用说明

1. 基于maven项目，从[http://git.oschina.net/mzllon/easy-okhttp-test-server](http://git.oschina.net/mzllon/easy-okhttp-test-server)fork到自己的账号下，然后通过git把项目clone下来。
2. 用idea等开发工具导入项目
3. 找到启动类`com.mzlion.test.easyokhttp.server.Application`，直接启动即可。
4. 然后可以允许相关测试方法了，所有的测试累在`src/test/java/com.mzlion.test.easyokhttp`

## 项目简介

项目采用标准的MVC模式，里面代码基本上都是有注释的，下面简单介绍每个package的功能。

```html
src
    main
        java
            com.mzlion.test.easyokhttp.server.config        mybatis配置文件
            com.mzlion.test.easyokhttp.server.controller    自定义的controller
            com.mzlion.test.easyokhttp.server.dto           dto
            com.mzlion.test.easyokhttp.server.entity        实体类
            com.mzlion.test.easyokhttp.server.mapper        类似于DAO
            com.mzlion.test.easyokhttp.server.service       service层
            com.mzlion.test.easyokhttp.server.Application   SpringBoot核心启动类
        resources
            mapper                                          mybatis的xml文件
            application.yaml                                SpringBoot核心配置文件(连接池,日志)
    test
        java
            com.mzlion.test.easyokhttp.TestHttps            提供了测试HTTPS
            com.mzlion.test.easyokhttp.TestUserInfoClient   模拟客户端发送请求
        resources                                           测试所需的资源
```

如果大家有什么疑问，可以在群里提问，我会及时回答的。        